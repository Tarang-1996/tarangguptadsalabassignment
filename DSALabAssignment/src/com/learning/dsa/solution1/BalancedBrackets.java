package com.learning.dsa.solution1;

import java.util.Stack;

/**
 * @author Tarang Gupta
 *
 */

public class BalancedBrackets {
	
	/**
	 * To check the balanced brackets
	 * 
	 * @param str   
	 * string of brackets
	 * 
	 * @return boolean
	 */

	public static boolean checkBalancedBrackets(String str) {

		Stack<Character> stack = new Stack<Character>();

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);

			switch (ch) {
			case '(':
				stack.push(')');
				break;
			case '{':
				stack.push('}');
				break;
			case '[':
				stack.push(']');
				break;
			default:
				break;
			}
			if ((ch == '}' || ch == ']' || ch == ')') && !stack.isEmpty()) {
				if (!stack.isEmpty() && stack.peek() == ch) {
					stack.pop();
				} else {
					return false;
				}
			}
			// to check the condition if closing brackets are more than opening brackets e.g. {}]]
			if (stack.isEmpty() && i < str.length() - 1) {
				return false;
			}

		}

		return stack.isEmpty();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "([[{}]]))";

		if (checkBalancedBrackets(str)) {
			System.out.println("The entered String has Balanced Brackets");
		} else {
			System.out.println("The entered Strings do not contain Balanced Brackets");
		}

	}

}
