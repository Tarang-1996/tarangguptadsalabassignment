package com.learning.dsa.solution2;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Tarang Gupta
 *
 */

public class BSTFindPair {

	// class to define the node structure
	static class Node {
		int data;
		Node left, right;
	}
	
	/**
	 * To create a new node
	 * 
	 * @param data   
	 * value of the node
	 * 
	 * @return Node
	 */
	static Node newNode(int data) {

		Node temp = new Node();
		temp.data = data;
		temp.left = null;
		temp.right = null;

		return temp;
	}
	
	/**
	 * To insert the node in the tree
	 * 
	 * @param root   
	 * root node of the tree
	 * 
	 * @param key   
	 * data of node to be inserted in the tree 
	 * 
	 * @return Node
	 */
	public Node insertNode(Node root, int key) {

		if (root == null) {
			return newNode(key);
		}
		if (key < root.data) {
			root.left = insertNode(root.left, key);
		} else {
			root.right = insertNode(root.right, key);
		}

		return root;

	}
	
	/**
	 * To find the pair nodes
	 * 
	 * @param root   
	 * root node of the tree
	 * 
	 * @param sum   
	 * sum of the pair
	 * 
	 * @param set   
	 * set to store the data of nodes
	 * 
	 * @return boolean
	 */

	public boolean findPair(Node root, int sum, Set<Integer> set) {

		if (root == null) {
			return false;
		}

		if (findPair(root.left, sum, set)) {
			return true;
		}

		if (set.contains(sum - root.data)) {
			System.out.println("Pair is (" + root.data + "," + (sum - root.data) + ")");
			return true;
		} else {
			set.add(root.data);
		}

		return findPair(root.right, sum, set);
	}

	/**
	 * To check if tree has the pair nodes
	 * 
	 * @param root   
	 * root node of the tree
	 * 
	 * @param sum   
	 * sum of the pair
	 *
	 * @return boolean
	 */
	public void findPairSum(Node root, int sum) {

		Set<Integer> set = new HashSet<Integer>();

		if (!findPair(root, sum, set)) {
			System.out.println("Pair does not exist");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Node root = null;
		BSTFindPair bstFindPair = new BSTFindPair();
		root = bstFindPair.insertNode(root, 40);
		root = bstFindPair.insertNode(root, 20);
		root = bstFindPair.insertNode(root, 10);
		root = bstFindPair.insertNode(root, 30);
		root = bstFindPair.insertNode(root, 50);
		root = bstFindPair.insertNode(root, 70);
		root = bstFindPair.insertNode(root, 60);

		int inputSum = 130;

		bstFindPair.findPairSum(root, inputSum);

	}

}
